using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoviesAPI.Entities;
using MoviesAPI.DTOs;
using Newtonsoft.Json;

namespace MoviesAPI.Tests.IntegrationTests
{
    public class ReviewsControllerTests : TestsBase
    {
        private static readonly string url = "/api/movies/1/reviews";

        [TestMethod]
        public async Task GetReviewsReturns404WhenMovieDoesNotExist() 
        {
        	var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);
            
            var client = factory.CreateClient();
            var response = await client.GetAsync(url);

            Assert.AreEqual(404, (int)response.StatusCode);
        }

        [TestMethod]
        public async Task GetReviewsReturnsEmptyList() 
        {
        	var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);
            var context = BuildContext(dbName);

            context.Movies.Add(new Movie() { Title = "Movie 1" });
            await context.SaveChangesAsync();
            var client = factory.CreateClient();
            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var reviews = JsonConvert
            	.DeserializeObject<List<ReviewDTO>>(await response.Content.ReadAsStringAsync());

            Assert.AreEqual(0, reviews.Count);
        }
    }
}
    