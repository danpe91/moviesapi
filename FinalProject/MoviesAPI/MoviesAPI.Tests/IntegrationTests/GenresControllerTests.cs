using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace MoviesAPI.Tests.IntegrationTests
{
    [TestClass]
    public class GenresControllerTests : TestsBase
    {
        private static readonly string url = "/api/genres";

        [TestMethod]
        public async Task GetAllGenresWithEmptyList() 
        {
        	var dbName = Guid.NewGuid().ToString();
        	var factory = BuildWebApplicationFactory(dbName);
            
            var client = factory.CreateClient();
            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var genres = JsonConvert
	            .DeserializeObject<List<Genre>>(await response.Content.ReadAsStringAsync());

	        Assert.AreEqual(0, genres.Count);
        }

        [TestMethod]
        public async Task GetAllGenres() 
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);
            var context = BuildContext(dbName);
            
            context.Genres.Add(new Genre() { Name = "Genre 1" });
            context.Genres.Add(new Genre() { Name = "Genre 2" });
            await context.SaveChangesAsync();
            
            var client = factory.CreateClient();
            var response = await client.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var genres = JsonConvert
                .DeserializeObject<List<Genre>>(await response.Content.ReadAsStringAsync());

            Assert.AreEqual(2, genres.Count);
        }

        [TestMethod]
        public async Task DeleteGenre() 
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName);
            var context = BuildContext(dbName);

            context.Genres.Add(new Genre() { Name = "Genre 1" });
            await context.SaveChangesAsync();
            
            var client = factory.CreateClient();
            var response = await client.DeleteAsync($"{url}/1 ");

            response.EnsureSuccessStatusCode();

            var context2 = BuildContext(dbName);

            var exists = await context2.Genres.AnyAsync();

            Assert.IsFalse(exists);
        }

        [TestMethod]
        public async Task DeleteGenreReturns401() 
        {
            var dbName = Guid.NewGuid().ToString();
            var factory = BuildWebApplicationFactory(dbName, false);

            var client = factory.CreateClient();
            var response = await client.DeleteAsync($"{url}/1 ");

            Assert.AreEqual("Unauthorized", response.ReasonPhrase);
        }
    }
}
