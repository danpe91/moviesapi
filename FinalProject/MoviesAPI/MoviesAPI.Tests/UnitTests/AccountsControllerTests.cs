using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.Controllers;
using MoviesAPI.Entities;
using MoviesAPI.DTOs;
using Moq;
using Microsoft.AspNetCore.Mvc;

namespace MoviesAPI.Tests.UnitTests
{
    [TestClass]
    public class AccountsControllerTests : TestsBase
    {
        [TestMethod]
        public async Task CreateUser() 
        {
            var dbName = Guid.NewGuid().ToString();
            
            await CreateUserHelper(dbName);
            var context2 = BuildContext(dbName);
            var count = await context2.Users.CountAsync();

            Assert.AreEqual(1, count);
        }

        [TestMethod]
        public async Task UserMustNotLogin() 
        {
            var dbName = Guid.NewGuid().ToString();
            await CreateUserHelper(dbName);

            var controller = BuildAccountsController(dbName);
            var userInfo = new UserInfo() { Email = "sample@email.com", Password = "WrongPassword" };
            var response = await controller.Login(userInfo);
            var result = response.Result as BadRequestObjectResult;

            Assert.IsNull(response.Value);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task UserMustLogin() 
        {
            var dbName = Guid.NewGuid().ToString();
            await CreateUserHelper(dbName);

            var controller = BuildAccountsController(dbName);
            var userInfo = new UserInfo() { Email = "sample@email.com", Password = "Some-Password1" };
            var response = await controller.Login(userInfo);

            Assert.IsNotNull(response.Value);
            Assert.IsNotNull(response.Value.Token);
        }

        private async Task CreateUserHelper(string dbName)
        {
            var accountsController = BuildAccountsController(dbName);
            var userInfo = new UserInfo() { Email = "sample@email.com", Password = "Some-Password1" };
            await accountsController.CreateUser(userInfo);
        }

        private AccountsController BuildAccountsController(string dbName) 
        {
            var context = BuildContext(dbName);
            var userStore = new UserStore<IdentityUser>(context);
            var userManager = BuildUserManager(userStore);
            var mapper = ConfigureAutoMapper();
            var httpContext = new DefaultHttpContext();

            MockAuth(httpContext);

            var signInManager = SetupSignInManager(userManager, httpContext);

            var myConfiguration = new Dictionary<string, string>
            {
                { "JWT:key", "HJKAKJD7DAS9DA9DA7ASD9ADS9DAS7DAS9ASD7SAD9ADS7DSA9ADS7DAS90DAS7KDAKIADS7" }
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();

            return new AccountsController(userManager, signInManager, configuration, context, mapper);
        }

        private UserManager<TUser> BuildUserManager<TUser>(IUserStore<TUser> store = null) where TUser : class
        {
            store = store ?? new Mock<IUserStore<TUser>>().Object;
            var options = new Mock<IOptions<IdentityOptions>>();
            var idOptions = new IdentityOptions();
            idOptions.Lockout.AllowedForNewUsers = false;

            options.Setup(o => o.Value).Returns(idOptions);

            var userValidators = new List<IUserValidator<TUser>>();

            var validator = new Mock<IUserValidator<TUser>>();
            userValidators.Add(validator.Object);
            var pwdValidators = new List<PasswordValidator<TUser>>();
            pwdValidators.Add(new PasswordValidator<TUser>());

            var userManager = new UserManager<TUser>(store, options.Object, new PasswordHasher<TUser>(),
                userValidators, pwdValidators, new UpperInvariantLookupNormalizer(),
                new IdentityErrorDescriber(), null,
                new Mock<ILogger<UserManager<TUser>>>().Object);

            validator.Setup(v => v.ValidateAsync(userManager, It.IsAny<TUser>()))
                .Returns(Task.FromResult(IdentityResult.Success)).Verifiable();

            return userManager;
        }

        private static SignInManager<TUser> SetupSignInManager<TUser>(UserManager<TUser> manager,
            HttpContext context, ILogger logger = null, IdentityOptions identityOptions = null,
            IAuthenticationSchemeProvider schemeProvider = null) where TUser : class
        {
            var contextAccessor = new Mock<IHttpContextAccessor>();
            contextAccessor.Setup(a => a.HttpContext).Returns(context);
            identityOptions = identityOptions ?? new IdentityOptions();
            var options = new Mock<IOptions<IdentityOptions>>();
            options.Setup(a => a.Value).Returns(identityOptions);
            var claimsFactory = new UserClaimsPrincipalFactory<TUser>(manager, options.Object);
            schemeProvider = schemeProvider ?? new Mock<IAuthenticationSchemeProvider>().Object;
            var sm = new SignInManager<TUser>(manager, contextAccessor.Object, claimsFactory, options.Object, null, schemeProvider, new DefaultUserConfirmation<TUser>());
            sm.Logger = logger ?? (new Mock<ILogger<SignInManager<TUser>>>()).Object;
            return sm;
        }
    
         private Mock<IAuthenticationService> MockAuth(HttpContext context)
        {
            var auth = new Mock<IAuthenticationService>();
            context.RequestServices = new ServiceCollection().AddSingleton(auth.Object).BuildServiceProvider();
            return auth;
        }
    }
}
    