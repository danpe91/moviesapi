using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetTopologySuite.Geometries;
using NetTopologySuite;
using MoviesAPI.Controllers;
using MoviesAPI.Entities;
using MoviesAPI.DTOs;
using Microsoft.EntityFrameworkCore;

namespace MoviesAPI.Tests.UnitTests
{
    [TestClass]
    public class CinemaRoomsControllerTests : TestsBase
    {
        [TestMethod]
        public async Task GetCineRoomsWithinFiveKMs() 
        {
            var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);

            using (var context = LocalDbDatabaseInitializer.GetDbContextLocalDb(false))
            {
                var cinemaRoom = new List<CinemaRoom>()
                {
                    new CinemaRoom { Name = "Cinema 5", Location = geometryFactory.CreatePoint(new Coordinate(-103.391609, 20.6416233))}
                };

                context.AddRange(cinemaRoom);
                await context.SaveChangesAsync();
            }

            var filter = new CinemaRoomCloseFilterDTO()
            {
                DistanceInKMs = 5,
                Latitude = 20.6310879,
                Longitude = -103.3873595
            };

            using (var context = LocalDbDatabaseInitializer.GetDbContextLocalDb(false))
            {
                var mapper = ConfigureAutoMapper();
                var controller = new CinemaRoomsController(context, mapper, geometryFactory);
                var cinemas = await context.CinemaRooms.ToListAsync();
                var response = await controller.CloseCinemaRooms(filter);
                var responseValue = response.Value;

                Assert.AreEqual(3, responseValue.Count);
            }
        }
    }
}
