using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoviesAPI.Controllers;
using MoviesAPI.Entities;
using MoviesAPI.DTOs;
using Microsoft.EntityFrameworkCore;

namespace MoviesAPI.Tests.UnitTests
{
    [TestClass]
    public class GenresControllerTests : TestsBase
    {
        [TestMethod]
        public async Task GetAllGenres() 
        {
            // Set up
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            context.Genres.Add(new Genre() { Name = "Genre 1"});
            context.Genres.Add(new Genre() { Name = "Genre 2"});

            await context.SaveChangesAsync();

            var context2 = BuildContext(dbName);

            // Test
            var controller = new GenresController(context2, mapper);
            var response = await controller.Get();

            // Verification
            var genres = response.Value;
            Assert.AreEqual(2, genres.Count);
        }

        [TestMethod]
        public async Task GetGenreByNonExistentId() 
        {
            // Set up
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            // Test
            var controller = new GenresController(context, mapper);
            var response = await controller.Get(1);

            // Verification
            var result = response.Result as StatusCodeResult;

            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public async Task GetGenreByExistentId() 
        {
            // Set up
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            context.Genres.Add(new Genre() { Name = "Genre 1"});
            context.Genres.Add(new Genre() { Name = "Genre 2"});
            await context.SaveChangesAsync();

            var context2 = BuildContext(dbName);

            // Test
            var controller = new GenresController(context2, mapper);
            var id = 1;
            var response = await controller.Get(id);

            // Verification
            var result = response.Value;

            Assert.AreEqual(id, result.Id);
        }

        [TestMethod]
        public async Task CreateGenre() 
        {
            // Set up
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            var newGenre = new GenreCreationDTO() { Name = "New Genre" };

            var controller = new GenresController(context, mapper);
            var response = await controller.Post(newGenre);
            var result = response as CreatedAtRouteResult;

            Assert.IsNotNull(result);

            var context2 = BuildContext(dbName);
            var quantity = await context2.Genres.CountAsync();
            Assert.AreEqual(1, quantity);
        }

        [TestMethod]
        public async Task UpdateGenre() 
        {
            // Set up
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            context.Genres.Add(new Genre() { Name = "Genre1" });
            await context.SaveChangesAsync();

            var context2 = BuildContext(dbName);
            var controller = new GenresController(context2, mapper);

            var genreCreationDTO = new GenreCreationDTO() { Name = "New name" };
            var id = 1;
            var response = await controller.Put(id, genreCreationDTO);

            var result = response as StatusCodeResult;
            Assert.AreEqual(204, result.StatusCode);

            var context3 = BuildContext(dbName);
            var exists = await context3.Genres.AnyAsync(x => x.Name == "New name");
            Assert.IsTrue(exists);
        }

        [TestMethod]
        public async Task DeleteNonExistent() 
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            var controller = new GenresController(context, mapper);

            var response = await controller.Delete(1);

            var result = response as StatusCodeResult;

            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public async Task DeleteExistent() 
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            context.Genres.Add(new Genre() { Name = "Genre1" });
            await context.SaveChangesAsync();

            var context2 = BuildContext(dbName);
            var controller = new GenresController(context2, mapper);

            var response = await controller.Delete(1);
            var result = response as StatusCodeResult;
            Assert.AreEqual(204, result.StatusCode);

            var context3 = BuildContext(dbName);
            var exists = await context3.Genres.AnyAsync();
            Assert.IsFalse(exists);
        }
    }
}
    