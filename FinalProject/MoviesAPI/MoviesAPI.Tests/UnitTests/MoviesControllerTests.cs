using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using MoviesAPI.Controllers;
using MoviesAPI.Entities;
using MoviesAPI.DTOs;
using Moq;

namespace MoviesAPI.Tests.UnitTests
{
	[TestClass]
    public class MoviesControllerTests : TestsBase
    {
        private string CreateTestData()
        {
        	var dbName = Guid.NewGuid().ToString();
        	var context = BuildContext(dbName);
        	var genre = new Genre() { Name = "Genre 1" };

        	var movies = new List<Movie>()
        	{
        		new Movie() { Title = "Movie 1", ReleaseDate = new DateTime(2010, 1, 1), InTheaters = false },
        		new Movie() { Title = "Future Release", ReleaseDate = DateTime.Today.AddDays(1), InTheaters = false },
        		new Movie() { Title = "Movie in Theaters", ReleaseDate = DateTime.Today.AddDays(-1), InTheaters = true }
        	};

        	var movieWithGenre = new Movie()
        	{
        		Title = "Movie with genre",
        		ReleaseDate = new DateTime(2010, 1, 1),
        		InTheaters = false
        	};

        	movies.Add(movieWithGenre);

        	context.Add(genre);
        	context.AddRange(movies);
        	context.SaveChanges();

        	var movieGenre = new MoviesGenres() { GenreId = genre.Id, MovieId = movieWithGenre.Id };
        	context.Add(movieGenre);
        	context.SaveChanges();

        	return dbName;
        }

	    [TestMethod]
	    public async Task FilterByTitle()
	    {
	    	var dbName = CreateTestData();
	    	var mapper = ConfigureAutoMapper();
	    	var context = BuildContext(dbName);

	    	var controller = new MoviesController(context, mapper, null, null);
	    	controller.ControllerContext.HttpContext = new DefaultHttpContext();

	    	var movieTitle = "Movie 1";

	    	var filterDTO = new FilterMoviesDTO()
	    	{
	    		Title = movieTitle,
	    		EntriesPerPage = 10
	    	};

	    	var response = await controller.Filter(filterDTO);
	    	var movies = response.Value;
	    	Assert.AreEqual(1, movies.Count);
	    	Assert.AreEqual(movieTitle, movies[0].Title);
	    }

	    [TestMethod]
	    public async Task FilterInTheatersMovies()
	    {
	    	var dbName = CreateTestData();
	    	var mapper = ConfigureAutoMapper();
	    	var context = BuildContext(dbName);

	    	var controller = new MoviesController(context, mapper, null, null);
	    	controller.ControllerContext.HttpContext = new DefaultHttpContext();

	    	var filterDTO = new FilterMoviesDTO()
	    	{
	    		InTheaters = true
	    	};

	    	var response = await controller.Filter(filterDTO);
	    	var movies = response.Value;
	    	Assert.AreEqual(1, movies.Count);
	    	Assert.AreEqual("Movie in Theaters", movies[0].Title);
	    }

	    [TestMethod]
	    public async Task FilterNextReleasesMovies()
	    {
	    	var dbName = CreateTestData();
	    	var mapper = ConfigureAutoMapper();
	    	var context = BuildContext(dbName);

	    	var controller = new MoviesController(context, mapper, null, null);
	    	controller.ControllerContext.HttpContext = new DefaultHttpContext();

	    	var filterDTO = new FilterMoviesDTO()
	    	{
	    		NextReleases = true
	    	};

	    	var response = await controller.Filter(filterDTO);
	    	var movies = response.Value;
	    	Assert.AreEqual(1, movies.Count);
	    	Assert.AreEqual("Future Release", movies[0].Title);
	    }

	    [TestMethod]
	    public async Task FilterMoviesByGenre()
	    {
	    	var dbName = CreateTestData();
	    	var mapper = ConfigureAutoMapper();
	    	var context = BuildContext(dbName);

	    	var controller = new MoviesController(context, mapper, null, null);
	    	controller.ControllerContext.HttpContext = new DefaultHttpContext();

	    	var genreId = context.Genres.Select(x => x.Id).First();

	    	var filterDTO = new FilterMoviesDTO()
	    	{
	    		GenreId = genreId
	    	};

	    	var response = await controller.Filter(filterDTO);
	    	var movies = response.Value;
	    	Assert.AreEqual(1, movies.Count);
	    	Assert.AreEqual("Movie with genre", movies[0].Title);
	    }

	    [TestMethod]
	    public async Task FilterOrderAscendingTitles()
	    {
	    	var dbName = CreateTestData();
	    	var mapper = ConfigureAutoMapper();
	    	var context = BuildContext(dbName);

	    	var controller = new MoviesController(context, mapper, null, null);
	    	controller.ControllerContext.HttpContext = new DefaultHttpContext();

	    	var filterDTO = new FilterMoviesDTO()
	    	{
	    		OrderField = "Title",
	    		AscendingOrder = true
	    	};

	    	var response = await controller.Filter(filterDTO);
	    	var movies = response.Value;
	    	
	    	var context2 = BuildContext(dbName);
	    	var moviesDB = context2.Movies.OrderBy(x => x.Title).ToList();

	    	Assert.AreEqual(moviesDB.Count, movies.Count);

	    	for (int i = 0; i < moviesDB.Count; i++) 
	    	{
	    		var movieFromController = movies[i];
	    		var movieDB = moviesDB[i];

	    		Assert.AreEqual(movieFromController.Id,movieDB.Id);
	    	}
	    }

	    [TestMethod]
	    public async Task FilterOrderDescendingTitles()
	    {
	    	var dbName = CreateTestData();
	    	var mapper = ConfigureAutoMapper();
	    	var context = BuildContext(dbName);

	    	var controller = new MoviesController(context, mapper, null, null);
	    	controller.ControllerContext.HttpContext = new DefaultHttpContext();

	    	var filterDTO = new FilterMoviesDTO()
	    	{
	    		OrderField = "Title",
	    		AscendingOrder = false
	    	};

	    	var response = await controller.Filter(filterDTO);
	    	var movies = response.Value;
	    	
	    	var context2 = BuildContext(dbName);
	    	var moviesDB = context2.Movies.OrderByDescending(x => x.Title).ToList();

	    	Assert.AreEqual(moviesDB.Count, movies.Count);

	    	for (int i = 0; i < moviesDB.Count; i++) 
	    	{
	    		var movieFromController = movies[i];
	    		var movieDB = moviesDB[i];

	    		Assert.AreEqual(movieFromController.Id,movieDB.Id);
	    	}
	    }

	    [TestMethod]
	    public async Task FilterByIncorrectFieldReturnsMovies()
	    {
	    	var dbName = CreateTestData();
	    	var mapper = ConfigureAutoMapper();
	    	var context = BuildContext(dbName);

	    	var mock = new Mock<ILogger<MoviesController>>();

	    	var controller = new MoviesController(context, mapper, null, mock.Object);
	    	controller.ControllerContext.HttpContext = new DefaultHttpContext();

	    	var genreId = context.Genres.Select(x => x.Id).First();

	    	var filterDTO = new FilterMoviesDTO()
	    	{
	    		OrderField = "NonExistentField",
	    		AscendingOrder = true
	    	};

	    	var response = await controller.Filter(filterDTO);
	    	var movies = response.Value;
	    	
	    	var context2 = BuildContext(dbName);
	    	var moviesDB = context2.Movies.ToList();

	    	Assert.AreEqual(movies.Count, moviesDB.Count);
	    	Assert.AreEqual(1, mock.Invocations.Count);
	    }
    }
}
    