using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using MoviesAPI.Controllers;
using MoviesAPI.Entities;
using MoviesAPI.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace MoviesAPI.Tests.UnitTests
{
    [TestClass]
    public class ReviewsControllerTests : TestsBase
    {
        [TestMethod]
        public async Task UserNotAllowedToCreateTwoReviewsOnSameMovie() 
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            CreateMovies(dbName);

            var movieId = context.Movies.Select(x => x.Id).First();
            var review1 = new Review() { MovieId = movieId, UserId = defaultUserId, Score = 5 };

            context.Add(review1);
            await context.SaveChangesAsync();
            
            var context2 = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();
            var controller = new ReviewController(context2, mapper);
            controller.ControllerContext = BuildControllerContext();

            var reviewCreationDTO = new ReviewCreationDTO { Score = 5 };

            var response = await controller.Post(movieId, reviewCreationDTO);

            var responseValue = response as IStatusCodeActionResult;
            Assert.AreEqual(400, responseValue.StatusCode.Value);
        }
        [TestMethod]
        public async Task CreateReview() 
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            CreateMovies(dbName);

            var movieId = context.Movies.Select(x => x.Id).First();
            var context2 = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();
            var controller = new ReviewController(context2, mapper);
            controller.ControllerContext = BuildControllerContext();

            var reviewCreationDTO = new ReviewCreationDTO { Score = 5 };
            var response = await controller.Post(movieId, reviewCreationDTO);

            var responseValue = response as NoContentResult;
            Assert.IsNotNull(responseValue);

            var context3 = BuildContext(dbName);
            var reviewDB = context3.Reviews.First();

            Assert.AreEqual(defaultUserId, reviewDB.UserId);
        }

        private void CreateMovies(string dbName)
        {
            var context = BuildContext(dbName);
            context.Movies.Add(new Movie() { Title = "Movie 1" });
            context.SaveChanges();
        }
    }
}
    