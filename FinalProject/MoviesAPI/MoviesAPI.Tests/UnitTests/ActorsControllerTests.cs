using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoviesAPI.Services;
using MoviesAPI.Controllers;
using MoviesAPI.Entities;
using MoviesAPI.DTOs;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Moq;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.JsonPatch.Operations;

namespace MoviesAPI.Tests.UnitTests
{
	[TestClass]
    public class ActorsControllerTests : TestsBase
    {
        [TestMethod]
        public async Task GetPaginatedActors() 
        {
            // Set up
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            context.Actors.Add(new Actor() { Name = "Actor 1"});
            context.Actors.Add(new Actor() { Name = "Actor 2"});
            context.Actors.Add(new Actor() { Name = "Actor 3"});
            await context.SaveChangesAsync();

            var context2 = BuildContext(dbName);

            // Test
            var controller = new ActorsController(context2, mapper, null);
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var page1 = await controller.Get(new PaginationDTO() { Page = 1, PageSize = 2 });
            var actorsPage1 = page1.Value;
            Assert.AreEqual(2, actorsPage1.Count);

            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var page2 = await controller.Get(new PaginationDTO() { Page = 2, PageSize = 2 });
            var actorsPage2 = page2.Value;
            Assert.AreEqual(1, actorsPage2.Count);

            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            var page3 = await controller.Get(new PaginationDTO() { Page = 3, PageSize = 2 });
            var actorsPage3 = page3.Value;
            Assert.AreEqual(0, actorsPage3.Count);
        }

        [TestMethod]
        public async Task CreateActorWithNoPhoto() 
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            var actor = new ActorCreationDTO() { Name = "John", BirthDate = DateTime.Now };

            var mock = new Mock<IFilesStorage>();
            mock.Setup(x => x.SaveFile(null, null, null, null))
                .Returns(Task.FromResult("url"));

            var controller = new ActorsController(context, mapper, mock.Object);
            var response = await controller.Post(actor);
            var result = response as CreatedAtRouteResult;
            Assert.AreEqual(201, result.StatusCode);

            var context2 = BuildContext(dbName);
            var list = await context2.Actors.ToListAsync();

            Assert.AreEqual(1, list.Count);
            Assert.IsNull(list[0].Photo);
            Assert.AreEqual(0, mock.Invocations.Count);
        }

        [TestMethod]
        public async Task CreateActorWithPhoto() 
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            var content = Encoding.UTF8.GetBytes("TestImage");
            var file = new FormFile(new MemoryStream(content), 0, content.Length, "Data", "image.jpg");
            file.Headers = new HeaderDictionary();
            file.ContentType = "image/jpg";

            var actor = new ActorCreationDTO()
            {
                Name = "John",
                BirthDate = DateTime.Now,
                Photo = file
            };

            var mock = new Mock<IFilesStorage>();
            mock.Setup(x => x.SaveFile(content, ".jpg", "actors", file.ContentType))
                .Returns(Task.FromResult("url"));

            var controller = new ActorsController(context, mapper, mock.Object);
            var response = await controller.Post(actor);
            var result = response as CreatedAtRouteResult;
            Assert.AreEqual(201, result.StatusCode);

            var context2 = BuildContext(dbName);
            var list = await context2.Actors.ToListAsync();
            Assert.AreEqual(1, list.Count);
            Assert.IsNotNull(list[0].Photo);
            Assert.AreEqual("url", list[0].Photo);
            Assert.AreEqual(1, mock.Invocations.Count);
        }

        [TestMethod]
        public async Task PatchReturns404IfActorDoesNotExist() 
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            var controller = new ActorsController(context, mapper, null);
            var patchDoc = new JsonPatchDocument<ActorPatchDTO>();
            var response = await controller.Patch(1, patchDoc);
            var result = response as StatusCodeResult;
            Assert.AreEqual(404, result.StatusCode);
        }

        [TestMethod]
        public async Task PatchUpdatesOnlyOneField()
        {
            var dbName = Guid.NewGuid().ToString();
            var context = BuildContext(dbName);
            var mapper = ConfigureAutoMapper();

            var birthDate = DateTime.Now;
            var actor = new Actor() { Name = "John", BirthDate = birthDate };
            context.Add(actor);
            await context.SaveChangesAsync();

            var context2 = BuildContext(dbName);
            var controller = new ActorsController(context2, mapper, null);

            var objectValidator = new Mock<IObjectModelValidator>();
            objectValidator.Setup(x => x.Validate(It.IsAny<ActionContext>(),
                It.IsAny<ValidationStateDictionary>(),
                It.IsAny<string>(),
                It.IsAny<object>()));

            controller.ObjectValidator = objectValidator.Object;

            var patchDoc = new JsonPatchDocument<ActorPatchDTO>();
            patchDoc.Operations.Add(new Operation<ActorPatchDTO>("replace", "/Name", null, "Peter"));
            var response = await controller.Patch(1, patchDoc);
            var result = response as StatusCodeResult;

            Assert.AreEqual(204, result.StatusCode);

            var context3 = BuildContext(dbName);
            var actorDB = await context3.Actors.FirstAsync();
            Assert.AreEqual("Peter", actorDB.Name);
            Assert.AreEqual(birthDate, actorDB.BirthDate);
        }
    }
}
