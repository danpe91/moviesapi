using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.DTOs;
using MoviesAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetTopologySuite.Geometries;

namespace MoviesAPI.Controllers
{
    [ApiController]
    [Route("api/cinemarooms")]
    public class CinemaRoomsController : CustomBaseController
    {
        private readonly ApplicationDBContext context;
        private readonly IMapper mapper;
        private readonly GeometryFactory geometryFactory;

        public CinemaRoomsController(ApplicationDBContext context, IMapper mapper, GeometryFactory geoMetryFactory) : base(context, mapper)
        {
            this.context = context;
            this.mapper = mapper;
            this.geometryFactory = geoMetryFactory;
        }

        [HttpGet]
        public async Task<ActionResult<List<CinemaRoomDTO>>>  Get()
        {
            return await Get<CinemaRoom, CinemaRoomDTO>();
        }

        [HttpGet("{id:int}", Name = "getCinemaRoom")]
        public async Task<ActionResult<CinemaRoomDTO>> Get(int id)
        {
            return await Get<CinemaRoom, CinemaRoomDTO>(id);
        }

        [HttpGet("Close")]
        public async Task<ActionResult<List<CinemaRoomCloseDTO>>> CloseCinemaRooms([FromQuery] CinemaRoomCloseFilterDTO filter)
        {
            var userLocation = geometryFactory.CreatePoint(new Coordinate(filter.Longitude, filter.Latitude));

            var distances = await context.CinemaRooms.Select(x => new { distance = x.Location.Distance(userLocation), distanceU = userLocation.Distance(x.Location) }).ToListAsync();

            var cinemaRooms = await context.CinemaRooms
                .OrderBy(x => x.Location.Distance(userLocation))
                .Where(x => x.Location.IsWithinDistance(userLocation, filter.DistanceInKMs * 1000))
                .Select(x => new CinemaRoomCloseDTO
                {
                    Id = x.Id,
                    Name = x.Name,
                    Latitude = x.Location.Y,
                    Longitude = x.Location.X,
                    DistanceInMts = Math.Round(x.Location.Distance(userLocation))
                })
                .ToListAsync();

            return cinemaRooms;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CinemaRoomCreationDTO newCinemaRoom)
        {
            return await Post<CinemaRoomCreationDTO, CinemaRoom, CinemaRoomDTO>(newCinemaRoom, "getCinemaRoom");
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] CinemaRoomCreationDTO updateCinemaRoom)
        {
            return await Put<CinemaRoomCreationDTO, CinemaRoom>(id, updateCinemaRoom);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            return await Delete<CinemaRoom>(id);
        }
    }
}
