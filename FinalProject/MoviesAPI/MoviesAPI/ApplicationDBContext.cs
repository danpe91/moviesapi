﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MoviesAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NetTopologySuite;
using NetTopologySuite.Geometries;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace MoviesAPI
{
    public class ApplicationDBContext : IdentityDbContext
    {
        public ApplicationDBContext( DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MoviesActors>().HasKey(x => new { x.ActorId, x.MovieId });
            modelBuilder.Entity<MoviesGenres>().HasKey(x => new { x.GenreId, x.MovieId });
            modelBuilder.Entity<MoviesCinemaRooms>().HasKey(x => new { x.MovieId, x.CinemaRoomId });
            
            SeedData(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            var adventure = new Genre() { Id = 4, Name = "Aventura" };
            var animation = new Genre() { Id = 5, Name = "Animación" };
            var suspense = new Genre() { Id = 6, Name = "Suspenso" };
            var romance = new Genre() { Id = 7, Name = "Romance" };

            modelBuilder.Entity<Genre>()
                .HasData(new List<Genre>
                {
                    adventure, animation, suspense, romance
                });

            var jimCarrey = new Actor() { Id = 5, Name = "Jim Carrey", BirthDate = new DateTime(1962, 01, 17) };
            var robertDowney = new Actor() { Id = 6, Name= "Robert Downey Jr.", BirthDate= new DateTime(1965, 4, 4) };
            var chrisEvans = new Actor() { Id = 7, Name = "Chris Evans", BirthDate= new DateTime(1981, 06, 13) };

            modelBuilder.Entity<Actor>()
                .HasData(new List<Actor>
                {
                    jimCarrey, robertDowney, chrisEvans
                });

            var endgame = new Movie()
            {
                Id = 2,
                Title = "Avengers: Endgame",
                InTheaters = true,
                ReleaseDate = new DateTime(2019, 04, 26)
            };

            var iw = new Movie()
            {
                Id = 3,
                Title = "Avengers: Infinity Wars",
                InTheaters = false,
                ReleaseDate= new DateTime(2019, 04, 26)
            };

            var sonic = new Movie()
            {
                Id = 4,
                Title = "Sonic the Hedgehog",
                InTheaters = false,
                ReleaseDate = new DateTime(2020, 02, 28)
            };
            var emma = new Movie()
            {
                Id = 5,
                Title = "Emma",
                InTheaters = false,
                ReleaseDate = new DateTime(2020, 02, 21)
            };
            var wonderwoman = new Movie()
            {
                Id = 6,
                Title = "Wonder Woman 1984",
                InTheaters = false,
                ReleaseDate = new DateTime(2020, 08, 14)
            };

            modelBuilder.Entity<Movie>()
                .HasData(new List<Movie>
                {
                    endgame, iw, sonic, emma, wonderwoman
                });

            modelBuilder.Entity<MoviesGenres>().HasData(
                new List<MoviesGenres>()
                {
                    new MoviesGenres(){MovieId = endgame.Id, GenreId = suspense.Id},
                    new MoviesGenres(){MovieId = endgame.Id, GenreId = adventure.Id},
                    new MoviesGenres(){MovieId = iw.Id, GenreId = suspense.Id},
                    new MoviesGenres(){MovieId = iw.Id, GenreId = adventure.Id},
                    new MoviesGenres(){MovieId = sonic.Id, GenreId = adventure.Id},
                    new MoviesGenres(){MovieId = emma.Id, GenreId = suspense.Id},
                    new MoviesGenres(){MovieId = emma.Id, GenreId = romance.Id},
                    new MoviesGenres(){MovieId = wonderwoman.Id, GenreId = suspense.Id},
                    new MoviesGenres(){MovieId = wonderwoman.Id, GenreId = adventure.Id},
                });

            modelBuilder.Entity<MoviesActors>().HasData(
                new List<MoviesActors>()
                {
                    new MoviesActors(){MovieId = endgame.Id, ActorId = robertDowney.Id, CharacterName = "Tony Stark", Order = 1},
                    new MoviesActors(){MovieId = endgame.Id, ActorId = chrisEvans.Id, CharacterName = "Steve Rogers", Order = 2},
                    new MoviesActors(){MovieId = iw.Id, ActorId = robertDowney.Id, CharacterName = "Tony Stark", Order = 1},
                    new MoviesActors(){MovieId = iw.Id, ActorId = chrisEvans.Id, CharacterName = "Steve Rogers", Order = 2},
                    new MoviesActors(){MovieId = sonic.Id, ActorId = jimCarrey.Id, CharacterName = "Dr. Ivo Robotnik", Order = 1}
                });

            var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid:4326);

            modelBuilder.Entity<CinemaRoom>().HasData(
                new List<CinemaRoom>
                {
                    new CinemaRoom{Id = 1, Name = "Cinema 1", Location = geometryFactory.CreatePoint(new Coordinate(-103.391869, 20.6425484))},
                    new CinemaRoom{Id = 2, Name = "Cinema 2", Location = geometryFactory.CreatePoint(new Coordinate(-103.391869, 20.6425484))},
                    new CinemaRoom{Id = 3, Name = "Cinema 3", Location = geometryFactory.CreatePoint(new Coordinate(-101.191976, 27.8933114))},
                    new CinemaRoom{Id = 4, Name = "Cinema 4", Location = geometryFactory.CreatePoint(new Coordinate(-101.838085, 33.544533))},
                });

            var roleAdminId = "fbe90121-a909-48df-99ad-e642a9dda497";
            var userAdminId = "9f0c65e7-b308-49a2-a4c4-d27bf0d861ea";

            var roleAdmin = new IdentityRole()
            {
                Id = roleAdminId,
                Name = "Admin",
                NormalizedName = "Admin"
            };

            var passwordHasher = new PasswordHasher<IdentityUser>();

            var username = "admin@email.com";

            var userAdmin = new IdentityUser()
            {
                Id = userAdminId,
                UserName = username,
                NormalizedUserName = username,
                Email = username,
                NormalizedEmail = username,
                PasswordHash = passwordHasher.HashPassword(null, "MySecurePassword")
            };

            // modelBuilder.Entity<IdentityUser>().HasData(userAdmin);
            // modelBuilder.Entity<IdentityRole>().HasData(roleAdmin);

            // modelBuilder.Entity<IdentityUserClaim<string>>().HasData(new IdentityUserClaim<string>()
            // {
            //     Id = 1,
            //     ClaimType = ClaimTypes.Role,
            //     UserId = userAdminId,
            //     ClaimValue = "Admin"
            // });
        }

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<CinemaRoom> CinemaRooms { get; set; }

        public DbSet<MoviesActors> MoviesActors { get; set; }
        public DbSet<MoviesGenres> MoviesGenres { get; set; }
        public DbSet<MoviesCinemaRooms> MoviesCinemaRooms { get; set; }
    }
}
