using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Entities
{
    public class MoviesCinemaRooms
    {
        public int MovieId { get; set; }
        public int CinemaRoomId { get; set; }
        public Movie Movie { get; set; }
        public CinemaRoom CinemaRoom { get; set; }
    }
}
