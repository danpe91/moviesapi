﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Validations
{
    public class FileTypeValidation : ValidationAttribute
    {
        private readonly string[] validFileTypes;

        public FileTypeValidation(string[] validFileTypes)
        {
            this.validFileTypes = validFileTypes;
        }

        public FileTypeValidation(FileTypeEnum fileType)
        {
            if (fileType == FileTypeEnum.Image)
            {
                validFileTypes = new string[] { "image/jpeg", "image/png", "image/gif" };
            }
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            IFormFile formFile = value as IFormFile;

            if (formFile == null)
            {
                return ValidationResult.Success;
            }

            if (!validFileTypes.Contains(formFile.ContentType))
            {
                return new ValidationResult($"El tipo del archivo debe ser uno de los siguientes: {string.Join(",", validFileTypes)}");
            }

            return ValidationResult.Success;
        }
    }
}
