﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Validations
{
    public class FileSizeValidation : ValidationAttribute
    {
        private readonly int maxSizeMB;

        public FileSizeValidation(int MaxSizeMB)
        {
            maxSizeMB = MaxSizeMB;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            IFormFile formFile = value as IFormFile;

            if (formFile == null)
            {
                return ValidationResult.Success;
            }

            if (formFile.Length > maxSizeMB * 1024 * 1024)
            {
                return new ValidationResult($"El peso del archivo no debe ser mayor a {maxSizeMB}mb");
            }
            return ValidationResult.Success;
        }
    }
}
