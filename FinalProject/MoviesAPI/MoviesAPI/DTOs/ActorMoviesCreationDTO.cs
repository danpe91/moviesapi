﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.DTOs
{
    public class ActorMoviesCreationDTO
    {
        public int ActorId { get; set; }
        public string CharacterName { get; set; }
    }
}
