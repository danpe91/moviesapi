﻿using Microsoft.AspNetCore.Http;
using MoviesAPI.Validations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.DTOs
{
    public class ActorCreationDTO
    {
        [Required]
        [StringLength(120)]
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        [FileSizeValidation(MaxSizeMB: 5)]
        [FileTypeValidation(fileType: FileTypeEnum.Image)]
        public IFormFile Photo { get; set; }
    }
}
