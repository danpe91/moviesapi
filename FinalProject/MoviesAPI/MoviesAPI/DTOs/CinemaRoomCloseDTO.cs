using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Entities
{
    public class CinemaRoomCloseDTO : CinemaRoomDTO
    {
        public double DistanceInMts { get; set; }
    }
}
