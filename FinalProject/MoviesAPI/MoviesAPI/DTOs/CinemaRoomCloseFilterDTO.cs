using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace MoviesAPI.Entities
{
    public class CinemaRoomCloseFilterDTO
    {
        [Range(-90,90)]
        public double Latitude { get; set; }
        [Range(-180,180)]
        public double Longitude { get; set; }
        private int distanceInKMs = 10;
        private int maxDistanceInKMs = 50;

        public int DistanceInKMs
        {
        	get
        	{
        		return distanceInKMs;
        	}
        	set
        	{
        		distanceInKMs = (value > maxDistanceInKMs) ? maxDistanceInKMs : value;
        	}
        }
    }
}
