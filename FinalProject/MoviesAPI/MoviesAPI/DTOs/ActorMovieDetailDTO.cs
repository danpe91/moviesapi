﻿namespace MoviesAPI.DTOs
{
    public class ActorMovieDetailDTO
    {
        public int ActorId { get; set; }
        public string CharacterName { get; set; }
        public string ActorName { get; set; }
    }
}