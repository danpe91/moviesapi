﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MoviesAPI.Migrations
{
    public partial class AdminData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
        	migrationBuilder.Sql(@"
        		IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'ConcurrencyStamp', N'Name', N'NormalizedName') AND [object_id] = OBJECT_ID(N'[AspNetRoles]'))
				    SET IDENTITY_INSERT [AspNetRoles] ON;
				INSERT INTO [AspNetRoles] ([Id], [ConcurrencyStamp], [Name], [NormalizedName])
				VALUES (N'fbe90121-a909-48df-99ad-e642a9dda497', N'2593d877-3ede-4a58-82e4-705c75f915b5', N'Admin', N'Admin');
				IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'ConcurrencyStamp', N'Name', N'NormalizedName') AND [object_id] = OBJECT_ID(N'[AspNetRoles]'))
				    SET IDENTITY_INSERT [AspNetRoles] OFF;
				GO

				IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AccessFailedCount', N'ConcurrencyStamp', N'Email', N'EmailConfirmed', N'LockoutEnabled', N'LockoutEnd', N'NormalizedEmail', N'NormalizedUserName', N'PasswordHash', N'PhoneNumber', N'PhoneNumberConfirmed', N'SecurityStamp', N'TwoFactorEnabled', N'UserName') AND [object_id] = OBJECT_ID(N'[AspNetUsers]'))
				    SET IDENTITY_INSERT [AspNetUsers] ON;
				INSERT INTO [AspNetUsers] ([Id], [AccessFailedCount], [ConcurrencyStamp], [Email], [EmailConfirmed], [LockoutEnabled], [LockoutEnd], [NormalizedEmail], [NormalizedUserName], [PasswordHash], [PhoneNumber], [PhoneNumberConfirmed], [SecurityStamp], [TwoFactorEnabled], [UserName])
				VALUES (N'9f0c65e7-b308-49a2-a4c4-d27bf0d861ea', 0, N'6c6a69a2-26ea-4395-8870-7dcc0422b6b0', N'admin@email.com', CAST(0 AS bit), CAST(0 AS bit), NULL, N'admin@email.com', N'admin@email.com', N'AQAAAAEAACcQAAAAEH/9cdDJQ+QAGvTgc3poFstfIlH0kLH2yQuPi4zew7ph+TH32doGDVgbaeQfAHVI9A==', NULL, CAST(0 AS bit), N'e4cec088-bc23-498d-a400-527dc1ce3bd0', CAST(0 AS bit), N'admin@email.com');
				IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'AccessFailedCount', N'ConcurrencyStamp', N'Email', N'EmailConfirmed', N'LockoutEnabled', N'LockoutEnd', N'NormalizedEmail', N'NormalizedUserName', N'PasswordHash', N'PhoneNumber', N'PhoneNumberConfirmed', N'SecurityStamp', N'TwoFactorEnabled', N'UserName') AND [object_id] = OBJECT_ID(N'[AspNetUsers]'))
				    SET IDENTITY_INSERT [AspNetUsers] OFF;
				GO

				IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'ClaimType', N'ClaimValue', N'UserId') AND [object_id] = OBJECT_ID(N'[AspNetUserClaims]'))
				    SET IDENTITY_INSERT [AspNetUserClaims] ON;
				INSERT INTO [AspNetUserClaims] ([Id], [ClaimType], [ClaimValue], [UserId])
				VALUES (1, N'http://schemas.microsoft.com/ws/2008/06/identity/claims/role', N'Admin', N'9f0c65e7-b308-49a2-a4c4-d27bf0d861ea');
				IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'ClaimType', N'ClaimValue', N'UserId') AND [object_id] = OBJECT_ID(N'[AspNetUserClaims]'))
				    SET IDENTITY_INSERT [AspNetUserClaims] OFF;
				GO
        	");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        	migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "fbe90121-a909-48df-99ad-e642a9dda497");

            migrationBuilder.DeleteData(
                table: "AspNetUserClaims",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9f0c65e7-b308-49a2-a4c4-d27bf0d861ea");
        }
    }
}
