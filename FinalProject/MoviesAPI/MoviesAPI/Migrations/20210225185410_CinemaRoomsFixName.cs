﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MoviesAPI.Migrations
{
    public partial class CinemaRoomsFixName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoviesCinemaRooms_CinemaRoom_CinemaRoomId",
                table: "MoviesCinemaRooms");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CinemaRoom",
                table: "CinemaRoom");

            migrationBuilder.RenameTable(
                name: "CinemaRoom",
                newName: "CinemaRooms");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CinemaRooms",
                table: "CinemaRooms",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MoviesCinemaRooms_CinemaRooms_CinemaRoomId",
                table: "MoviesCinemaRooms",
                column: "CinemaRoomId",
                principalTable: "CinemaRooms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MoviesCinemaRooms_CinemaRooms_CinemaRoomId",
                table: "MoviesCinemaRooms");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CinemaRooms",
                table: "CinemaRooms");

            migrationBuilder.RenameTable(
                name: "CinemaRooms",
                newName: "CinemaRoom");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CinemaRoom",
                table: "CinemaRoom",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MoviesCinemaRooms_CinemaRoom_CinemaRoomId",
                table: "MoviesCinemaRooms",
                column: "CinemaRoomId",
                principalTable: "CinemaRoom",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
