﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using MoviesAPI.DTOs;
using MoviesAPI.Entities;
using NetTopologySuite;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles(GeometryFactory geometryFactory)
        {
            CreateMap<Genre, GenreDTO>().ReverseMap();
            CreateMap<GenreCreationDTO, Genre>();

            CreateMap<Review, ReviewDTO>()
                .ForMember(x => x.UserName, x => x.MapFrom(y => y.User.UserName));
            CreateMap<ReviewDTO, Review>();
            CreateMap<ReviewCreationDTO, Review>();

            CreateMap<IdentityUser, UserDTO>();
            CreateMap<CinemaRoom, CinemaRoomDTO>()
                .ForMember(x => x.Latitude, x => x.MapFrom(y => y.Location.Y))
                .ForMember(x => x.Longitude, x => x.MapFrom(y => y.Location.X));

            CreateMap<CinemaRoomDTO, CinemaRoom>()
                .ForMember(x => x.Location, x => x.MapFrom(y => 
                    geometryFactory.CreatePoint(new Coordinate(y.Longitude, y.Latitude))));

            CreateMap<CinemaRoomCreationDTO, CinemaRoom>()
                .ForMember(x => x.Location, x => x.MapFrom(y => 
                    geometryFactory.CreatePoint(new Coordinate(y.Longitude, y.Latitude))));
            
            CreateMap<Actor, ActorDTO>().ReverseMap();
            CreateMap<ActorCreationDTO, Actor>().ForMember(x => x.Photo, options => options.Ignore());
            CreateMap<ActorPatchDTO, Actor>().ReverseMap();

            CreateMap<Movie, MovieDTO>().ReverseMap();
            CreateMap<MovieCreationDTO, Movie>()
                .ForMember(x => x.Poster, options => options.Ignore())
                .ForMember(x => x.MoviesGenres, options => options.MapFrom(MapMoviesGenres))
                .ForMember(x => x.MoviesActors, options => options.MapFrom(MapMoviesActors));
            CreateMap<MoviePatchDTO, Movie>().ReverseMap();

            CreateMap<Movie, MovieDetailsDTO>()
                .ForMember(x => x.Genres, options => options.MapFrom(MapMoviesGenres))
                .ForMember(x => x.Actors, options => options.MapFrom(MapMoviesActors));
        }

        private List<ActorMovieDetailDTO> MapMoviesActors(Movie movie, MovieDetailsDTO movieDetailsDTO)
        {
            var result = new List<ActorMovieDetailDTO>();

            if (movie.MoviesActors == null) { return result; }

            foreach(var actorMovie in movie.MoviesActors)
            {
                result.Add(new ActorMovieDetailDTO
                {
                    ActorId = actorMovie.ActorId,
                    CharacterName = actorMovie.CharacterName,
                    ActorName = actorMovie.Actor.Name
                });
            }

            return result;
        }

        private List<GenreDTO> MapMoviesGenres(Movie movie, MovieDetailsDTO movieDetailsDTO)
        {
            var result = new List<GenreDTO>();

            if (movie.MoviesGenres == null) { return result; }

            foreach(var genreMovie in movie.MoviesGenres)
            {
                result.Add(new GenreDTO() { Id = genreMovie.GenreId, Name = genreMovie.Genre.Name });
            }

            return result;
        }

        private List<MoviesGenres> MapMoviesGenres(MovieCreationDTO movieCreationDTO, Movie movie)
        {
            var result = new List<MoviesGenres>();

            if(movieCreationDTO.GenresIds == null) { return result; }

            foreach(var id in movieCreationDTO.GenresIds)
            {
                result.Add(new MoviesGenres() { GenreId = id });
            }

            return result;
        }

        private List<MoviesActors> MapMoviesActors(MovieCreationDTO movieCreationDTO, Movie movie)
        {
            var result = new List<MoviesActors>();

            if (movieCreationDTO.Actors == null) { return result; }

            foreach (var actor in movieCreationDTO.Actors)
            {
                result.Add(new MoviesActors() { ActorId = actor.ActorId, CharacterName = actor.CharacterName });
            }

            return result;
        }
    }
}
