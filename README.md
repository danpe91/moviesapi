# Repositorio del curso: Construyendo Web APIs con ASP.NET Core 3.1

Link del curso: https://www.udemy.com/course/construyendo-web-apis-restful-con-aspnet-core/?referralCode=2067EC056E505823483D

En este repositorio encontrarás el código fuente de lo enseñado en el curso de desarrollo de Web APIs con ASP.NET Core 3.1

En la carpeta FinalProject está el código que personalmente fui generando para el proyecto final (última lección del curso) siguiendo los videos. Dentro de esta carpeta se encuentran también archivos de docker ya que personalmente implementé docker y docker compose dentro del proyecto

Gracias!
